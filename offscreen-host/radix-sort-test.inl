#include <algorithm>
#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

#include <glcpp/ShaderProgram.hpp>
#include <glcpp/ShaderStorageBufferObject.hpp>

#include <gpgl/Context.hpp>
#include <gpgl/device/radix_sort.hpp>


void deviceRadixSortTest(gpgl::Context& gpCtx, bool useSigned)
{
	unsigned bits = unsigned(std::round(std::log(float(RAND_MAX)) / std::log(2.0f)));
	if (useSigned)
		std::cout << "deviceRadixSortTest (signed 32-bit values)" << std::endl;
	else
		std::cout << "deviceRadixSortTest (unsigned " << bits << "-bit values)" << std::endl;

	constexpr GLsizeiptr kProblemSizes[] = { 11, 16, 19, 21, 500, 1000, 1024, 1031, 1679, 1931, 2048, 10'000, 100'000, 1'000'000, 10'000'000, 50'000'000 };

	for (const auto& problemSize : kProblemSizes)
	{
		std::cout << "\tproblem size: " << problemSize << std::endl;
		std::vector<GLint> input(problemSize);
		//std::iota(input.begin(), input.end(), -10000);
		std::generate(input.begin(), input.end(), [&]
		{
			if (useSigned)
				return std::rand() - RAND_MAX / 2;
			else
				return std::rand();
		});
		std::vector<GLint> output(input);

		const auto t1 = std::chrono::high_resolution_clock::now();
		std::sort(output.begin(), output.end());
		const auto t2 = std::chrono::high_resolution_clock::now();

		glcpp::ShaderStorageBufferObject<GLint> keysBuffer(input);
		glcpp::ShaderStorageBufferObject<GLint> indicesBuffer(input.size());

		GLuint query;
		glGenQueries(1, &query);
		glcpp::checkGl();
		glBeginQuery(GL_TIME_ELAPSED, query);
		glcpp::checkGl();

		glcpp::ShaderStorageBufferObject<GLint> extraStorage(gpgl::device::radix_sort::extraStorageSize(problemSize));

		if (useSigned)
		{
			gpgl::device::radix_sort::dispatch(gpCtx, keysBuffer, indicesBuffer, extraStorage);
		}
		else
		{
			gpgl::device::radix_sort::dispatch(gpCtx, keysBuffer, indicesBuffer, extraStorage, bits, bits + 1);
		}
		
		glEndQuery(GL_TIME_ELAPSED);
		glcpp::checkGl();

		keysBuffer.map([&](const GLint* keys)
		{
			if (!std::is_sorted(keys, keys + problemSize))
			{
				std::cout << "\t\tFAILED" << std::endl;
				std::terminate();
			}
		});

		indicesBuffer.map([&](const GLint* indices)
		{
			for (GLsizeiptr i = 0; i < problemSize; ++i)
			{
				auto oldPos = indices[i];
				if (input[oldPos] != output[i])
				{
					std::cout << "\t\tFAILED" << std::endl;
					std::terminate();
				}
			}
		});


		GLuint64 nanoseconds;
		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &nanoseconds);
		glcpp::checkGl();
		glDeleteQueries(1, &query);
		glcpp::checkGl();

		std::cout << "\t\tCPU: " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << " us" << std::endl;
		std::cout << "\t\tGPU: " << nanoseconds / 1000 << " us" << std::endl;
	}
}

void radixSortTest(gpgl::Context& gpCtx)
{
	deviceRadixSortTest(gpCtx, true);
	deviceRadixSortTest(gpCtx, false);
}