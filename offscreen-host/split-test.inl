#include <algorithm>
#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

#include <glcpp/ShaderProgram.hpp>
#include <glcpp/ShaderStorageBufferObject.hpp>

#include <gpgl/Context.hpp>
#include <gpgl/device/split.hpp>


GPGL_REGISTER_COMPUTE_SHADER(device_split_test_predicate, GPGL_SHADER_CODE(
layout(local_size_x = 1024) in;

uniform uint kProblemSize;

layout(std430, binding = 0) buffer Input
{
	int data[];
};

layout(std430, binding = 1) buffer Output
{
	int result[];
};

void main()
{
	if (gl_GlobalInvocationID.x >= kProblemSize)
		return;
	result[gl_GlobalInvocationID.x] = data[gl_GlobalInvocationID.x] % 7 == 0 ? 0 : 1;
}
));

GPGL_REGISTER_COMPUTE_SHADER(device_split_test_scatter, GPGL_SHADER_CODE(
layout(local_size_x = 1024) in;
uniform uint kProblemSize;
layout(std430, binding = 0) buffer Input
{
	int data[];
};

layout(std430, binding = 1) buffer Indices
{
	int indices[];
};

layout(std430, binding = 2) buffer Output
{
	int result[];
};

void main()
{
	if (gl_GlobalInvocationID.x >= kProblemSize)
		return;
	result[indices[gl_GlobalInvocationID.x]] = data[gl_GlobalInvocationID.x];
}
));

void deviceSplitTest(gpgl::Context& gpCtx)
{
	std::cout << "deviceSplitTest:" << std::endl;
	constexpr GLsizeiptr kProblemSizes[] = { 11, 16, 19, 21, 500, 1000, 1024, 1031, 1679, 1931, 2048, 10'000, 100'000, 1'000'000, 10'000'000, 50'000'000 };

	for (const auto& problemSize : kProblemSizes)
	{
		std::cout << "\tproblem size: " << problemSize << std::endl;
		std::vector<GLint> input(problemSize);
		std::vector<GLint> output(problemSize);

		std::iota(input.begin(), input.end(), 0);

		const auto t1 = std::chrono::high_resolution_clock::now();
		std::for_each(input.cbegin(), input.cend(), [it1 = output.begin(), it2 = output.rbegin()](const GLint& v) mutable
		{
			if (v % 7 == 0)
			{
				*it1 = v;
				++it1;
			}
			else
			{
				*it2 = v;
				++it2;
			}
		});
		const auto t2 = std::chrono::high_resolution_clock::now();

		glcpp::ShaderStorageBufferObject<GLint> inputBuffer(input);
		glcpp::ShaderStorageBufferObject<GLint> maskBuffer(input.size());
		glcpp::ShaderStorageBufferObject<GLint> indicesBuffer(input.size());
		glcpp::ShaderStorageBufferObject<GLint> resultBuffer(input.size());

		GLuint query;
		glGenQueries(1, &query);
		glcpp::checkGl();
		glBeginQuery(GL_TIME_ELAPSED, query);
		glcpp::checkGl();

		inputBuffer.bind(0, [&]
		{
			maskBuffer.bind(1, [&]
			{
				auto& predicateShader = gpCtx.shader("device_split_test_predicate");
				predicateShader.bind([&]
				{
					predicateShader.setUniform("kProblemSize", GLuint(input.size()));

					const auto workgroups = gpgl::divceil<GLuint>(GLuint(input.size()), 1024);
					glDispatchCompute(workgroups, 1, 1);
					glcpp::checkGl();
				});
			});
		});

		const auto extraStorageSize = gpgl::device::split::extraStorageSize(problemSize);
		glcpp::ShaderStorageBufferObject<GLint> extraStorage(extraStorageSize);

		gpgl::device::split::dispatch(gpCtx, maskBuffer, indicesBuffer, extraStorage);

		inputBuffer.bind(0, [&]
		{
			indicesBuffer.bind(1, [&]
			{
				resultBuffer.bind(2, [&]
				{
					auto& gatherShader = gpCtx.shader("device_split_test_scatter");
					gatherShader.bind([&]
					{
						gatherShader.setUniform("kProblemSize", GLuint(input.size()));

						const auto workgroups = gpgl::divceil<GLuint>(GLuint(input.size()), 1024);
						glDispatchCompute(workgroups, 1, 1);
						glcpp::checkGl();
					});
				});
			});
		});
		glEndQuery(GL_TIME_ELAPSED);
		glcpp::checkGl();

		resultBuffer.map([&](const GLint* data)
		{
			for (GLsizeiptr i = 0; i < problemSize; ++i)
			{
				bool leftGroup1 = data[i] % 7 == 0;
				bool leftGroup2 = output[i] % 7 == 0;
				if (leftGroup1 != leftGroup2)
				{
					std::cout << "\t\tFAILED" << std::endl;
					std::terminate();
				}
			}

		});

		GLuint64 nanoseconds;
		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &nanoseconds);
		glcpp::checkGl();		
		glDeleteQueries(1, &query);
		glcpp::checkGl();
		
		std::cout << "\t\tCPU: " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << " us" << std::endl;
		std::cout << "\t\tGPU: " << nanoseconds / 1000 << " us" << std::endl;
	}
}

void splitTest(gpgl::Context& gpCtx)
{
	deviceSplitTest(gpCtx);
}