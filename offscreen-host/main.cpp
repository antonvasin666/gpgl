#ifdef USE_GLEW
#include <GL/glew.h>
#endif

#include <GLFW/glfw3.h>

#include <gpgl/Context.hpp>

#include "inclusive-scan-test.inl"
#include "radix-sort-test.inl"
#include "split-test.inl"

int main()
{
	glfwInit();

	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
	GLFWwindow* offscreen_ctx = glfwCreateWindow(800, 600, "", nullptr, nullptr);
	glfwMakeContextCurrent(offscreen_ctx);

#ifdef USE_GLEW
	glewInit();
#endif

	std::cout << "Vendor:\t\t" << glGetString(GL_VENDOR) << std::endl;
	std::cout << "Renderer:\t" << glGetString(GL_RENDERER) << std::endl;
	std::cout << "OpenGL version:\t" << glGetString(GL_VERSION) << std::endl;
	std::cout << std::endl;

	{
		gpgl::Context gpCtx;

		inclusiveScanTest(gpCtx);
		splitTest(gpCtx);
		radixSortTest(gpCtx);
	}

	glfwDestroyWindow(offscreen_ctx);
	glfwTerminate();
	return 0;
}