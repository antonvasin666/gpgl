cmake_minimum_required(VERSION 3.9)

add_subdirectory(third-party/glfw)

project(offscreen-host)

set (SRC
  main.cpp
  
  inclusive-scan-test.inl
  radix-sort-test.inl
  split-test.inl
)

source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}" FILES ${SRC})
add_executable(${PROJECT_NAME} ${SRC})
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 17)
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD_REQUIRED true)

target_link_libraries(${PROJECT_NAME} PUBLIC gpgl glfw)