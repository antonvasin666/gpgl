#include <algorithm>
#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

#include <glcpp/ShaderProgram.hpp>
#include <glcpp/ShaderStorageBufferObject.hpp>

#include <gpgl/Context.hpp>
#include <gpgl/block/inclusive_scan.hpp>
#include <gpgl/device/inclusive_scan.hpp>

void blockInclusiveScanTest(gpgl::Context& gpCtx)
{
	std::cout << "blockInclusiveScanTest: block size: " << gpgl::block::inclusive_sum::blockSize << std::endl;
	constexpr GLsizeiptr kProblemSizes[] = { 1, 2, 3, 5, 7, 11, 16, 19, 21, 500, 1000, 1024, 1031, 1679, 1931, 2048, 10'000, 100'000, 1'000'000, 10'000'000, 50'000'000};
	for (const auto& problemSize : kProblemSizes)
	{
		std::cout << "\tproblem size: " << problemSize << std::endl;
		std::vector<GLint> input(problemSize);

		std::iota(input.begin(), input.end(), -1'000'000);

		glcpp::ShaderStorageBufferObject<GLint> inputBuffer(input);
		glcpp::ShaderStorageBufferObject<GLint> outputBuffer(input.size());

		GLuint query;
		glGenQueries(1, &query);
		glcpp::checkGl();
		glBeginQuery(GL_TIME_ELAPSED, query);
		glcpp::checkGl();
		
		gpgl::block::inclusive_sum::dispatch(gpCtx, inputBuffer, 0, inputBuffer.size(), outputBuffer, 0);
		
		glEndQuery(GL_TIME_ELAPSED);
		glcpp::checkGl();
		
		outputBuffer.map([&](const GLint* data)
		{
			GLint counter = 0;
			for (GLsizeiptr i = 0; i < problemSize; ++i)
			{
				if (i % (gpgl::block::inclusive_sum::blockSize) == 0)
				{
					counter = 0;
				}
				counter += input[i];

				if (data[i] != counter)
				{
					std::cout << "\t\tFAILED" << std::endl;
					std::terminate();
				}
			}
			
		});
		GLuint64 nanoseconds;
		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &nanoseconds);
		glcpp::checkGl();
		glDeleteQueries(1, &query);
		glcpp::checkGl();

		std::cout << "\t\tGPU: " << nanoseconds / 1000 << " us" << std::endl;;

	}
}

void deviceInclusiveScanTest(gpgl::Context& gpCtx)
{
	std::cout << "deviceInclusiveScanTest"<< std::endl;

	constexpr GLsizeiptr kProblemSizes[] = { 2000, 2048, 3000, 4096, 5000, 6000, 8192, 10'000, 100'000, 1'000'000, 10'000'000, 50'000'000 };
	for (const auto& problemSize : kProblemSizes)
	{
		std::cout << "\tproblem size: " << problemSize << std::endl;
		std::vector<GLint> input(problemSize);
		std::vector<GLint> output(problemSize);

		std::iota(input.begin(), input.end(), -1'000'000);
		/*std::generate(input.begin(), input.end(), []
		{
			return std::rand() % 11 - 5;
		});*/

		const auto t1 = std::chrono::high_resolution_clock::now();
		std::inclusive_scan(input.cbegin(), input.cend(), output.begin());
		const auto t2 = std::chrono::high_resolution_clock::now();

		glcpp::ShaderStorageBufferObject<GLint> inputBuffer(input);
		glcpp::ShaderStorageBufferObject<GLint> outputBuffer(input.size());

		const auto extraStorageSize = gpgl::device::inclusive_sum::extraStorageSize(problemSize);
		glcpp::ShaderStorageBufferObject<GLint> extraStorage(extraStorageSize);

		GLuint query;
		glGenQueries(1, &query);
		glcpp::checkGl();
		glBeginQuery(GL_TIME_ELAPSED, query);
		glcpp::checkGl();

		gpgl::device::inclusive_sum::dispatch(gpCtx, inputBuffer, outputBuffer, extraStorage);
		
		glEndQuery(GL_TIME_ELAPSED);
		glcpp::checkGl();

		outputBuffer.map([&](const GLint* data)
		{
			for (GLsizeiptr i = 0; i < problemSize; ++i)
			{
				if (data[i] != output[i])
				{
					std::cout << "\t\tFAILED" << std::endl;
					std::terminate();
				}
			}

		});
		GLuint64 nanoseconds;
		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &nanoseconds);
		glcpp::checkGl();
		glDeleteQueries(1, &query);
		glcpp::checkGl();
		std::cout << "\t\tCPU: " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << " us" << std::endl;
		std::cout << "\t\tGPU: " << nanoseconds / 1000 << " us" << std::endl;
	}
}

void inclusiveScanTest(gpgl::Context& gpCtx)
{
	blockInclusiveScanTest(gpCtx);
	deviceInclusiveScanTest(gpCtx);
}