#include <gpgl/block/inclusive_scan.hpp>

namespace gpgl::block::inclusive_sum
{

const GLuint blockSize = 128 * 2;		//<<<<<
const GLuint workgroupSize = 128;		//<<<<<
GPGL_REGISTER_COMPUTE_SHADER(block_inclusive_sum, GPGL_SHADER_CODE(
layout(local_size_x = 128) in;			//<<<<<
shared int[gl_WorkGroupSize.x * 2u] temp;

uniform uint kProblemSize;
uniform uint kInOffset;
uniform uint kOutOffset;

layout(std430, binding = 0) buffer Input
{
	restrict readonly int data[];
};

layout(std430, binding = 1) buffer Output
{
	restrict writeonly int result[];
};

uint upSweepPhase(uint logStep, uint offset)
{
	barrier();

	if (gl_LocalInvocationID.x < logStep)
	{
		uint ai = offset * (gl_LocalInvocationID.x * 2u + 1u) - 1u;
		uint bi = offset * (gl_LocalInvocationID.x * 2u + 2u) - 1u;

		temp[bi] += temp[ai];
	}

	return logStep > 0u ? offset << 1u : offset;
}

uint downSweepPhase(uint logStep, uint offset, uint blockSize)
{
	barrier();

	offset >>= 1u;

	if (gl_LocalInvocationID.x < logStep && logStep < blockSize)
	{
		uint bi = offset * (gl_LocalInvocationID.x * 2u + 1u) - 1u;
		if (offset < bi)
		{
			uint ai = bi - offset;
			temp[bi] += temp[ai];
		}
	}

	return offset;
}

void main()
{
	uint blockOffset = gl_WorkGroupID.x * gl_WorkGroupSize.x * 2u;
	uint gIdx1 = blockOffset + gl_LocalInvocationID.x;
	uint gIdx2 = gIdx1 + gl_WorkGroupSize.x;
	uint blockSize = gl_WorkGroupSize.x * 2u;
	if (blockOffset + blockSize > kProblemSize)
		blockSize = kProblemSize - blockOffset;

	//Intel hangs on subsequent barriers if some threads exit
	//if (gIdx1 >= kProblemSize)
	//	return;

	//prefetch
	temp[gl_LocalInvocationID.x] = gIdx1 < kProblemSize ? data[kInOffset + gIdx1] : 0;
	temp[gl_LocalInvocationID.x + gl_WorkGroupSize.x] = gIdx2 < kProblemSize ? data[kInOffset + gIdx2] : 0;

	//Intel compiler doesn't like 'for' loops with barrier() calls inside
	uint offset = 1u;
	uint logStep = blockSize;						//logStep = 256 (may be different)
	offset = upSweepPhase(logStep >>= 1u, offset);	//logStep = 128
	offset = upSweepPhase(logStep >>= 1u, offset);	//logStep = 64
	offset = upSweepPhase(logStep >>= 1u, offset);	//logStep = 32
	offset = upSweepPhase(logStep >>= 1u, offset);	//logStep = 16
	offset = upSweepPhase(logStep >>= 1u, offset);	//logStep = 8
	offset = upSweepPhase(logStep >>= 1u, offset);	//logStep = 4
	offset = upSweepPhase(logStep >>= 1u, offset);	//logStep = 2
	offset = upSweepPhase(logStep >>= 1u, offset);	//logStep = 1

	barrier();

	//if blockSize is power of 2
	if ((blockSize & (blockSize - 1u)) == 0u)
	{
		offset >>= 1u;
	}

	logStep = 1u;
	offset = downSweepPhase(logStep <<= 1u, offset, blockSize); //logStep = 2
	offset = downSweepPhase(logStep <<= 1u, offset, blockSize); //logStep = 4
	offset = downSweepPhase(logStep <<= 1u, offset, blockSize); //logStep = 8
	offset = downSweepPhase(logStep <<= 1u, offset, blockSize); //logStep = 16
	offset = downSweepPhase(logStep <<= 1u, offset, blockSize); //logStep = 32
	offset = downSweepPhase(logStep <<= 1u, offset, blockSize); //logStep = 64
	offset = downSweepPhase(logStep <<= 1u, offset, blockSize); //logStep = 128

	barrier();

	//unload
	if (gIdx1 < kProblemSize)
	{
		result[kOutOffset + gIdx1] = temp[gl_LocalInvocationID.x];
	}
	if (gIdx2 < kProblemSize)
	{
		result[kOutOffset + gIdx2] = temp[gl_LocalInvocationID.x + gl_WorkGroupSize.x];
	}
}

));

void dispatch(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& in,
	GLsizeiptr inOffset,
	GLsizeiptr inSize,
	glcpp::ShaderStorageBufferObject<GLint>& out,
	GLsizeiptr outOffset
)
{
	glcpp::ensure(inOffset + inSize <= in.size());
	glcpp::ensure(outOffset + inSize <= out.size());
	if (inSize == 0)
		return;

	in.bind(0, [&]
	{
		out.bind(1, [&]
		{
			auto& computeShader = ctx.shader("block_inclusive_sum");
			computeShader.bind([&]
			{
				computeShader.setUniform("kProblemSize", GLuint(inSize));
				computeShader.setUniform("kInOffset", GLuint(inOffset));
				computeShader.setUniform("kOutOffset", GLuint(outOffset));

				const auto workgroups = divceil<GLuint>(GLuint(inSize), blockSize);
				dispatch1D(workgroups);
			});
		});
	});
}
}