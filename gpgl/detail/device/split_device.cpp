#include <gpgl/device/inclusive_scan.hpp>
#include <gpgl/device/split.hpp>

namespace gpgl::device::split
{

GPGL_REGISTER_COMPUTE_SHADER(device_split_copy_total_count, GPGL_SHADER_CODE(
layout(local_size_x = 1) in;

uniform uint kInOffset;
uniform uint kOutOffset;

layout(std430, binding = 0) buffer DeviceScan
{
	int data[];
};

layout(std430, binding = 1) buffer Output
{
	int extra[];
};

void main()
{
	if (gl_GlobalInvocationID.x == 0u)
	{
		extra[kOutOffset] = data[kInOffset];
	}
}
));

const GLuint workgroupSize = 128;	//<<<<<
GPGL_REGISTER_COMPUTE_SHADER(device_split_compute_indices, GPGL_SHADER_CODE(
layout(local_size_x = 128) in;		//<<<<<

uniform uint kProblemSize;
uniform uint kScanOffset;
uniform uint kMaskOffset;
uniform uint kTotalCountOffset;

layout(std430, binding = 0) buffer DeviceScan
{
	int data[];
};

layout(std430, binding = 1) buffer TotalCount
{
	int extra[];
};

layout(std430, binding = 2) buffer Mask
{
	int mask[];
};

void main()
{
	if (gl_GlobalInvocationID.x >= kProblemSize)
		return;

	int flag = mask[kMaskOffset + gl_GlobalInvocationID.x];
	int totalCount = extra[kTotalCountOffset];
	int leftGroupSize = int(kProblemSize) - totalCount;
	int result = data[kScanOffset + gl_GlobalInvocationID.x];

	if (flag != 0)
	{
		result = result + leftGroupSize - 1;
	}
	else
	{
		result = int(gl_GlobalInvocationID.x) - result;
	}

	data[kScanOffset + gl_GlobalInvocationID.x] = result;
}
));


GLsizeiptr extraStorageSize(GLsizeiptr problemSize)
{
	return device::inclusive_sum::extraStorageSize(problemSize)
		+ 1; //need one more element to store sum of flags
}

void dispatch(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& inMask,
	GLsizeiptr inMaskOffset,
	GLsizeiptr inMaskSize,
	glcpp::ShaderStorageBufferObject<GLint>& outIndices,
	GLsizeiptr outIndicesOffset,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage,
	GLsizeiptr extraStorageOffset)
{
	const auto extraSize = extraStorageSize(inMaskSize);

	glcpp::ensure(inMaskOffset + inMaskSize <= inMask.size());
	glcpp::ensure(outIndicesOffset + inMaskSize <= outIndices.size());
	glcpp::ensure(extraStorageOffset + extraSize <= extraStorage.size());

	device::inclusive_sum::dispatch(ctx, inMask, inMaskOffset, inMaskSize, outIndices, outIndicesOffset, extraStorage, extraStorageOffset);

	outIndices.bind(0, [&]
	{
		extraStorage.bind(1, [&]
		{
			auto& copyShader = ctx.shader("device_split_copy_total_count");
			copyShader.bind([&]
			{
				copyShader.setUniform("kInOffset", GLuint(outIndicesOffset + inMaskSize - 1));
				copyShader.setUniform("kOutOffset", GLuint(extraStorageOffset + extraSize - 1));

				dispatch1D(1);
			});

			inMask.bind(2, [&]
			{
				auto& computeIndicesShader = ctx.shader("device_split_compute_indices");
				computeIndicesShader.bind([&]
				{
					computeIndicesShader.setUniform("kProblemSize", GLuint(inMaskSize));
					computeIndicesShader.setUniform("kScanOffset", GLuint(outIndicesOffset));
					computeIndicesShader.setUniform("kMaskOffset", GLuint(inMaskOffset));
					computeIndicesShader.setUniform("kTotalCountOffset", GLuint(extraStorageOffset + extraSize - 1));
					const auto workgroups = divceil<GLuint>(GLuint(inMaskSize), device::split::workgroupSize);
					dispatch1D(workgroups);
				});
			});
		});
	});
}
}
