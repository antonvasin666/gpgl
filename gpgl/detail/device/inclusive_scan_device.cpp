#include <gpgl/block/inclusive_scan.hpp>
#include <gpgl/device/inclusive_scan.hpp>

namespace gpgl::device::inclusive_sum
{

const GLuint workgroupSize = 128;	//<<<<<
GPGL_REGISTER_COMPUTE_SHADER(device_inclusive_sum_gather, GPGL_SHADER_CODE(
layout(local_size_x = 128) in;		//<<<<<

uniform uint kProblemSize;
uniform uint kBlockSize;
uniform uint kBlocks;
uniform uint kInOffset;
uniform uint kOutOffset;

layout(std430, binding = 0) buffer Input
{
	restrict readonly int data[];
};

layout(std430, binding = 1) buffer Output
{
	restrict writeonly int result[];
};

void main()
{
	if (gl_GlobalInvocationID.x >= kBlocks)
		return;

	uint blockLastElementIdx = (gl_GlobalInvocationID.x + 1u) * kBlockSize - 1u;
	if (blockLastElementIdx >= kProblemSize)
		blockLastElementIdx = kProblemSize - 1u;

	result[gl_GlobalInvocationID.x + kOutOffset] = data[blockLastElementIdx + kInOffset];
}
));

GPGL_REGISTER_COMPUTE_SHADER(device_inclusive_sum_map, GPGL_SHADER_CODE(
layout(local_size_x = 128) in; //<<<<<

uniform uint kProblemSize;
uniform uint kBlockSize;
uniform uint kInOffset;
uniform uint kOutOffset;

layout(std430, binding = 0) buffer BlockScans
{
	restrict int data[];
};

layout(std430, binding = 1) buffer Extra
{
	restrict readonly int extra[];
};

void main()
{
	if (gl_GlobalInvocationID.x + kBlockSize >= kProblemSize)
		return;

	uint blockIdx = gl_GlobalInvocationID.x / kBlockSize;
	data[kOutOffset + gl_GlobalInvocationID.x + kBlockSize] += extra[kInOffset + blockIdx];
}
));

static GLsizeiptr extraStorageSizeImpl(GLsizeiptr problemSize)
{
	const auto tails = divceil<GLsizeiptr>(problemSize, block::inclusive_sum::blockSize);
	if (tails <= block::inclusive_sum::blockSize)
		return tails * 2;

	return tails * 2 + extraStorageSizeImpl(tails);
};

GLsizeiptr extraStorageSize(GLsizeiptr problemSize)
{
	if (problemSize <= block::inclusive_sum::blockSize)
		return 0;

	return extraStorageSizeImpl(problemSize);
}

static void recursiveInclusiveSumScan(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& in,
	GLsizeiptr inOffset,
	GLsizeiptr inSize,
	glcpp::ShaderStorageBufferObject<GLint>& intermediate,
	GLsizeiptr intermediateOffset,
	glcpp::ShaderStorageBufferObject<GLint>& out,
	GLsizeiptr outOffset
)
{
	block::inclusive_sum::dispatch(ctx,
		in, inOffset, inSize,
		intermediate, intermediateOffset);
	const auto blocks = divceil<GLsizeiptr>(inSize, block::inclusive_sum::blockSize);
	if (blocks == 1)
		return;

	intermediate.bind(0, [&]
	{
		out.bind(1, [&]
		{
			auto& gatherShader = ctx.shader("device_inclusive_sum_gather");
			gatherShader.bind([&]
			{
				gatherShader.setUniform("kProblemSize", GLuint(inSize));
				gatherShader.setUniform("kBlockSize", GLuint(block::inclusive_sum::blockSize));
				gatherShader.setUniform("kBlocks", GLuint(blocks));
				gatherShader.setUniform("kInOffset", GLuint(intermediateOffset));
				gatherShader.setUniform("kOutOffset", GLuint(outOffset));


				const auto workgroups = divceil<GLuint>(GLuint(blocks), device::inclusive_sum::workgroupSize);
				dispatch1D(workgroups);
			});
		});
	});

	const auto mapOffset = outOffset;
	const auto scanMapOffset = outOffset + blocks;

	recursiveInclusiveSumScan(ctx,
		out, mapOffset, blocks,
		out, scanMapOffset,
		out, scanMapOffset + blocks);

	intermediate.bind(0, [&]
	{
		out.bind(1, [&]
		{
			auto& mapShader = ctx.shader("device_inclusive_sum_map");
			mapShader.bind([&]
			{
				mapShader.setUniform("kProblemSize", GLuint(inSize));
				mapShader.setUniform("kBlockSize", GLuint(block::inclusive_sum::blockSize));
				mapShader.setUniform("kInOffset", GLuint(scanMapOffset));
				mapShader.setUniform("kOutOffset", GLuint(intermediateOffset));

				const auto workgroups = divceil<GLuint>(GLuint(inSize) - block::inclusive_sum::blockSize, workgroupSize);
				dispatch1D(workgroups);
			});
		});
	});
}

void dispatch(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& in,
	GLsizeiptr inOffset,
	GLsizeiptr inSize,
	glcpp::ShaderStorageBufferObject<GLint>& out,
	GLsizeiptr outOffset,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage,
	GLsizeiptr extraStorageOffset)
{
	if (inSize == 0)
		return;

	const auto extraSize = extraStorageSize(inSize);

	glcpp::ensure(inOffset + inSize <= in.size());
	glcpp::ensure(outOffset + inSize <= out.size());
	glcpp::ensure(extraStorageOffset + extraSize <= extraStorage.size());

	recursiveInclusiveSumScan(ctx,
		in, inOffset, inSize,
		out, outOffset,
		extraStorage, extraStorageOffset);
}

}