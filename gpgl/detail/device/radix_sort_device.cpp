#include <gpgl/device/radix_sort.hpp>
#include <gpgl/device/split.hpp>

namespace gpgl::device::radix_sort
{

const GLuint workgroupSize = 128;	//<<<<<
GPGL_REGISTER_COMPUTE_SHADER(device_radix_sort_init_indices, GPGL_SHADER_CODE(
layout(local_size_x = 128) in;		//<<<<<

uniform uint kProblemSize;

layout(std430, binding = 0) buffer Indices
{
	int indices[];
};

void main()
{
	if (gl_GlobalInvocationID.x >= kProblemSize)
		return;
	indices[gl_GlobalInvocationID.x] = int(gl_GlobalInvocationID.x);
}
));

GPGL_REGISTER_COMPUTE_SHADER(device_radix_sort_map_mask, GPGL_SHADER_CODE(
layout(local_size_x = 128) in;		//<<<<<

uniform uint kProblemSize;
uniform uint kKeysOffset;
uniform uint kMaskOffset;
uniform uint kCurrentBit;
uniform uint kSignBit;

layout(std430, binding = 0) buffer Keys
{
	int keys[];
};

layout(std430, binding = 1) buffer Mask
{
	int mask[];
};

void main()
{
	if (gl_GlobalInvocationID.x >= kProblemSize)
		return;

	int key = keys[kKeysOffset + gl_GlobalInvocationID.x];
	bool bit = (key & (1 << kCurrentBit)) != 0;
	if (kSignBit == kCurrentBit)
		bit = !bit;

	mask[kMaskOffset + gl_GlobalInvocationID.x] = bit ? 1 : 0;
}
));

GPGL_REGISTER_COMPUTE_SHADER(device_radix_sort_scatter_keys, GPGL_SHADER_CODE(
layout(local_size_x = 128) in;		//<<<<<

uniform uint kProblemSize;
uniform uint kPrevKeysOffset;
uniform uint kPrevIndicesOffset;
uniform uint kSplitIndicesOffset;
uniform uint kNextKeysOffset;
uniform uint kNextIndicesOffset;

layout(std430, binding = 0) buffer PrevKeys
{
	int prevKeys[];
};
layout(std430, binding = 1) buffer PrevIndices
{
	int prevIndices[];
};

layout(std430, binding = 2) buffer SplitIndices
{
	int splitIndices[];
};

layout(std430, binding = 3) buffer NextKeys
{
	int nextKeys[];
};

layout(std430, binding = 4) buffer NextIndices
{
	int nextIndices[];
};

void main()
{
	if (gl_GlobalInvocationID.x >= kProblemSize)
		return;
	uint newPos = uint(splitIndices[kSplitIndicesOffset + gl_GlobalInvocationID.x]);
	nextKeys[kNextKeysOffset + newPos] = prevKeys[kPrevKeysOffset + gl_GlobalInvocationID.x];
	nextIndices[kNextIndicesOffset + newPos] = prevIndices[kPrevIndicesOffset + gl_GlobalInvocationID.x];
}
));

GPGL_REGISTER_COMPUTE_SHADER(device_radix_sort_copy_output, GPGL_SHADER_CODE(
layout(local_size_x = 128) in;		//<<<<<

uniform uint kProblemSize;
uniform uint kPrevKeysOffset;
uniform uint kPrevIndicesOffset;
uniform uint kNextKeysOffset;
uniform uint kNextIndicesOffset;

layout(std430, binding = 0) buffer PrevKeys
{
	int prevKeys[];
};
layout(std430, binding = 1) buffer PrevIndices
{
	int prevIndices[];
};

layout(std430, binding = 2) buffer NextKeys
{
	int nextKeys[];
};

layout(std430, binding = 3) buffer NextIndices
{
	int nextIndices[];
};

void main()
{
	if (gl_GlobalInvocationID.x >= kProblemSize)
		return;
	nextKeys[kNextKeysOffset + gl_GlobalInvocationID.x] = prevKeys[kPrevKeysOffset + gl_GlobalInvocationID.x];
	nextIndices[kNextIndicesOffset + gl_GlobalInvocationID.x] = prevIndices[kPrevIndicesOffset + gl_GlobalInvocationID.x];
}
));

GLsizeiptr extraStorageSize(GLsizeiptr problemSize)
{
	return
		problemSize + //newKeys
		problemSize + //newIndices
		problemSize + //mask
		problemSize + //indices (split output)
		device::split::extraStorageSize(problemSize); //extra memory for split
}

struct SortBuffer
{
	glcpp::ShaderStorageBufferObject<GLint>* buffer;
	GLuint offset;
};

void dispatch(
	Context& ctx,
	glcpp::ShaderStorageBufferObject<GLint>& keys,
	GLsizeiptr keysOffset,
	GLsizeiptr keysSize,
	glcpp::ShaderStorageBufferObject<GLint>& indices,
	GLsizeiptr indicesOffset,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage,
	GLsizeiptr extraStorageOffset,
	unsigned processBits,
	unsigned signBit)
{
	if (keysSize == 0)
		return;
	const auto extraSize = extraStorageSize(keysSize);
	const auto problemSize = GLuint(keysSize);
	glcpp::ensure(keysOffset + keysSize <= keys.size());
	glcpp::ensure(indicesOffset + keysSize <= indices.size());
	glcpp::ensure(extraStorageOffset + extraSize <= extraStorage.size());

	SortBuffer keysInBuffer = { &keys, GLuint(keysOffset) };
	SortBuffer indicesInBuffer = { &indices, GLuint(indicesOffset) };
	SortBuffer keysOutBuffer = { &extraStorage, 0 };
	SortBuffer indicesOutBuffer = { &extraStorage, keysOutBuffer.offset + problemSize };
	SortBuffer maskBuffer = { &extraStorage, indicesOutBuffer.offset + problemSize };
	SortBuffer splitIndicesBuffer = { &extraStorage, maskBuffer.offset + problemSize };
	SortBuffer splitExtraBuffer = { &extraStorage, splitIndicesBuffer.offset + problemSize };

	indicesInBuffer.buffer->bind(0, [&]
	{
		auto& initShader = ctx.shader("device_radix_sort_init_indices");
		initShader.bind([&]
		{
			initShader.setUniform("kProblemSize", problemSize);
			dispatch1D(divceil<GLuint>(problemSize, workgroupSize));
		});
	});

	for (unsigned char bit = 0; bit < processBits; ++bit)
	{
		keysInBuffer.buffer->bind(0, [&]
		{
			maskBuffer.buffer->bind(1, [&]
			{
				auto& maskShader = ctx.shader("device_radix_sort_map_mask");
				maskShader.bind([&]
				{
					maskShader.setUniform("kProblemSize", problemSize);
					maskShader.setUniform("kKeysOffset", keysInBuffer.offset);
					maskShader.setUniform("kMaskOffset", maskBuffer.offset);
					maskShader.setUniform("kCurrentBit", GLuint(bit));
					maskShader.setUniform("kSignBit", GLuint(signBit));
					dispatch1D(divceil<GLuint>(problemSize, workgroupSize));
				});
			});
		});

		device::split::dispatch(ctx, *maskBuffer.buffer, maskBuffer.offset, keysSize, *splitIndicesBuffer.buffer, splitIndicesBuffer.offset, *splitExtraBuffer.buffer, splitExtraBuffer.offset);

		keysInBuffer.buffer->bind(0, [&]
		{
			indicesInBuffer.buffer->bind(1, [&]
			{
				splitIndicesBuffer.buffer->bind(2, [&]
				{
					keysOutBuffer.buffer->bind(3, [&]
					{
						indicesOutBuffer.buffer->bind(4, [&]
						{
							auto& scatterShader = ctx.shader("device_radix_sort_scatter_keys");
							scatterShader.bind([&]
							{
								scatterShader.setUniform("kProblemSize", problemSize);
								scatterShader.setUniform("kPrevKeysOffset", keysInBuffer.offset);
								scatterShader.setUniform("kPrevIndicesOffset", indicesInBuffer.offset);
								scatterShader.setUniform("kSplitIndicesOffset", splitIndicesBuffer.offset);
								scatterShader.setUniform("kNextKeysOffset", keysOutBuffer.offset);
								scatterShader.setUniform("kNextIndicesOffset", indicesOutBuffer.offset);

								dispatch1D(divceil<GLuint>(problemSize, workgroupSize));
							});
						});
					});
				});
			});
		});

		std::swap(keysInBuffer, keysOutBuffer);
		std::swap(indicesInBuffer, indicesOutBuffer);
	}

	if (keysInBuffer.buffer == &keys)
		return;

	keysInBuffer.buffer->bind(0, [&]
	{
		indicesInBuffer.buffer->bind(1, [&]
		{
			keysOutBuffer.buffer->bind(2, [&]
			{
				indicesOutBuffer.buffer->bind(3, [&]
				{
					auto& copyShader = ctx.shader("device_radix_sort_copy_output");
					copyShader.bind([&]
					{
						copyShader.setUniform("kProblemSize", problemSize);
						copyShader.setUniform("kPrevKeysOffset", keysInBuffer.offset);
						copyShader.setUniform("kPrevIndicesOffset", indicesInBuffer.offset);
						copyShader.setUniform("kNextKeysOffset", keysOutBuffer.offset);
						copyShader.setUniform("kNextIndicesOffset", indicesOutBuffer.offset);

						dispatch1D(divceil<GLuint>(problemSize, workgroupSize));
					});
				});
			});
		});
	});
}
}