#include <gpgl/Context.hpp>

namespace gpgl
{
glcpp::ShaderProgram& Context::shader(const std::string& name) const
{
	auto it = m_shaders.find(name);
	
	if (it == m_shaders.end())
	{
		auto factoryIt = s_factories().find(name);
		if (factoryIt == s_factories().end())
			throw std::runtime_error("cannot instantiate shader");

		it = m_shaders.emplace(name, factoryIt->second()).first;
	}

	return it->second;
}

void Context::registerShaderFactory(std::string name, std::function<glcpp::ShaderProgram()> factory)
{
	s_factories().emplace(std::move(name), std::move(factory));
}

Context::ComputeShaderRegistrator::ComputeShaderRegistrator(std::string name, const GLchar* sourceCode)
{
	Context::registerShaderFactory(std::move(name), [sourceCode]
	{
		return glcpp::ShaderProgram(glcpp::ComputeShader::create(sourceCode));
	});
}

std::map<std::string, std::function<glcpp::ShaderProgram()>>& Context::s_factories()
{
	static std::map<std::string, std::function<glcpp::ShaderProgram()>> factories;
	return factories;
}

}