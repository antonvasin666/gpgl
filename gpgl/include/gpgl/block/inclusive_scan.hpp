#pragma once

#include <gpgl/Context.hpp>
#include <glcpp/ShaderStorageBufferObject.hpp>

namespace gpgl::block::inclusive_sum
{
extern const GLuint workgroupSize;
extern const GLuint blockSize;
void dispatch(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& in,
	GLsizeiptr inOffset,
	GLsizeiptr inSize,
	glcpp::ShaderStorageBufferObject<GLint>& out,
	GLsizeiptr outOffset
);
}