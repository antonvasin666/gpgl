#pragma once

#include <gpgl/Context.hpp>
#include <glcpp/ShaderStorageBufferObject.hpp>

namespace gpgl::device::inclusive_sum
{
extern const GLuint workgroupSize;

GLsizeiptr extraStorageSize(GLsizeiptr problemSize);

void dispatch(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& in,
	GLsizeiptr inOffset,
	GLsizeiptr inSize,
	glcpp::ShaderStorageBufferObject<GLint>& out,
	GLsizeiptr outOffset,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage,
	GLsizeiptr extraStorageOffset);

inline void dispatch(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& in,
	glcpp::ShaderStorageBufferObject<GLint>& out,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage)
{
	dispatch(ctx, in, 0, in.size(), out, 0, extraStorage, 0);
}
}