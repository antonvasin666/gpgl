#pragma once

#include <gpgl/Context.hpp>
#include <glcpp/ShaderStorageBufferObject.hpp>

namespace gpgl::device::radix_sort
{
extern const GLuint workgroupSize;

GLsizeiptr extraStorageSize(GLsizeiptr problemSize);

void dispatch(
	Context& ctx,
	glcpp::ShaderStorageBufferObject<GLint>& keys,
	GLsizeiptr keysOffset,
	GLsizeiptr keysSize,
	glcpp::ShaderStorageBufferObject<GLint>& indices,
	GLsizeiptr indicesOffset,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage,
	GLsizeiptr extraStorageOffset,
	unsigned processBits = 32,
	unsigned signBit = 31);

inline void dispatch(
	Context& ctx,
	glcpp::ShaderStorageBufferObject<GLint>& keys,
	glcpp::ShaderStorageBufferObject<GLint>& indices,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage,
	unsigned processBits = 32,
	unsigned signBit = 31)
{
	dispatch(ctx, keys, 0, keys.size(), indices, 0, extraStorage, 0, processBits, signBit);
}
}