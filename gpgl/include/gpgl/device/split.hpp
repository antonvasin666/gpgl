#pragma once

#include <gpgl/Context.hpp>
#include <glcpp/ShaderStorageBufferObject.hpp>

namespace gpgl::device::split
{
extern const GLuint workgroupSize;

GLsizeiptr extraStorageSize(GLsizeiptr problemSize);

void dispatch(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& inMask,
	GLsizeiptr inMaskOffset,
	GLsizeiptr inMaskSize,
	glcpp::ShaderStorageBufferObject<GLint>& outIndices,
	GLsizeiptr outIndicesOffset,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage,
	GLsizeiptr extraStorageOffset
);

inline void dispatch(
	Context& ctx,
	const glcpp::ShaderStorageBufferObject<GLint>& inMask,
	glcpp::ShaderStorageBufferObject<GLint>& outIndices,
	glcpp::ShaderStorageBufferObject<GLint>& extraStorage)
{
	dispatch(ctx, inMask, 0, inMask.size(), outIndices, 0, extraStorage, 0);
}
}