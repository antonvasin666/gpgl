#pragma once

#include <functional>
#include <map>
#include <string>
#include <utility>

#include <glcpp/ShaderProgram.hpp>

namespace gpgl
{

class Context
{
public:
	struct ComputeShaderRegistrator
	{
		ComputeShaderRegistrator(std::string name, const GLchar* sourceCode);
	};

	glcpp::ShaderProgram& shader(const std::string& name) const;

	static void registerShaderFactory(std::string name, std::function<glcpp::ShaderProgram()> factory);
private:
	mutable std::map<std::string, glcpp::ShaderProgram> m_shaders;

	static std::map<std::string, std::function<glcpp::ShaderProgram()>>& s_factories();
};

template <typename Integer>
Integer divceil(Integer numerator, Integer denominator)
{
	return (numerator - 1) / denominator + 1;
}

inline void dispatch1D(GLuint workgroups, GLbitfield barriers = GL_SHADER_STORAGE_BARRIER_BIT)
{
#ifdef _DEBUG
	GLint maxWorkgroups;
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &maxWorkgroups);
	glcpp::ensure(workgroups < GLuint(maxWorkgroups));
#endif

	if(barriers != 0)
	{
		glMemoryBarrier(barriers);
		glcpp::checkGl();
	}
#ifdef _DEBUG
	glFinish();
	glcpp::checkGl();
#endif
	glDispatchCompute(workgroups, 1, 1);
	glcpp::checkGl();
#ifdef _DEBUG
	glFinish();
	glcpp::checkGl();
#endif
}

}

#if defined(_WINDOWS)
	#define GPGL_SHADER_CODE(...) "#version 430\n" #__VA_ARGS__
#else
	#define GPGL_SHADER_CODE(...) "#version 310 es\n" #__VA_ARGS__
#endif

#define GPGL_REGISTER_COMPUTE_SHADER(NAME, CODE) static gpgl::Context::ComputeShaderRegistrator __##NAME(#NAME, CODE)